package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.Entreprise;
import com.time2it.entrepriseservice.repository.EntrepriseRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@Transactional
public class EntrepriseService {

    private final EntrepriseRepository entrepriseRepository;
    private final MailService mailService;

    @Transactional
    public Entreprise create(Entreprise entreprise) {
        Entreprise newEntreprise = entrepriseRepository.save(entreprise);
        //mailService.sendMail("Nouvelle demande d'inscription");
        return newEntreprise;
    }
}
