package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.Entreprise;
import com.time2it.entrepriseservice.entity.Role;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
@AllArgsConstructor
public class MailContentBuilder {

    private final TemplateEngine templateEngine;

    public String buildRegistrationMail() {
        Context context = new Context();
        //context.setVariable("message", var);
        return templateEngine.process("RegistrationMail", context);
    }
    
    public String buildAdminMail(Entreprise entr) {
        Context context = new Context();
        context.setVariable("entreprise",entr);
        return templateEngine.process("adminMail", context);
    }
    
    public String buildNewEntrepriseUserMail(String email, String password, String entreprise, List<Role> roles) {
        Context context = new Context();
        context.setVariable("email", email);
        context.setVariable("password", password);
        context.setVariable("entreprise", entreprise);
        context.setVariable("roles", roles);
        return templateEngine.process("InviteUserMail", context);
    }
    
    public String buildEntrepriseStatutEditionMail(String body, String btn) {
        Context context = new Context();
        context.setVariable("body", body);
        context.setVariable("btn", btn);
        return templateEngine.process("EntrepriseStatutMail", context);
    }

}
