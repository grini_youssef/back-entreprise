package com.time2it.entrepriseservice.services;

import com.time2it.entrepriseservice.entity.EmailHost;
import com.time2it.entrepriseservice.entity.Entreprise;
import com.time2it.entrepriseservice.entity.Role;
import com.time2it.entrepriseservice.exception.EntrepriseException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

@Service
@AllArgsConstructor
@Slf4j
public class MailService {

    private final JavaMailSender mailSender;
    private final MailContentBuilder mailContentBuilder;

    //public final String pathFileEmailConfig = Thread.currentThread().getContextClassLoader().getResource("").getPath() + "email.properties";
    //public final String pathFileEmailHostConfig = Thread.currentThread().getContextClassLoader().getResource("").getPath() + "application.properties";


    public String getAdminEmail(){
        String sendTo = "";
        try {
            File file = ResourceUtils.getFile("classpath:email.properties");
            FileInputStream in = new FileInputStream(file);
            Properties props = new Properties();
            props.load(in);
            sendTo = props.getProperty("emailTime2it");
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return sendTo;
    }

    @Async
    public void sendAdminMail(Entreprise entr) {
        MimeMessagePreparator messagePreparator = mimeMessage -> {
            MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
            messageHelper.setTo(this.getAdminEmail());
            messageHelper.setSubject("Nouvelle demande d'inscription");
            messageHelper.setText(mailContentBuilder.buildAdminMail(entr),true);
        };
        try {
            mailSender.send(messagePreparator);
            log.info("Email sent successfully !!");
            log.info(getAdminEmail());
        }
        catch (MailException e) {
            log.error("Exception occurred when sending mail", e);
            throw new EntrepriseException("Exception occurred when sending mail to ${emailTime2it} ", e);
        }
    }
   
    @Async
    public void sendRegistrationMail(String userMail) {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(userMail);
                messageHelper.setSubject("Bienvenue dans Time2it Group");
                messageHelper.setText(mailContentBuilder.buildRegistrationMail(),true);
            };
            try {
                mailSender.send(messagePreparator);
                log.info("Email sent successfully !!");
                //log.info(getEmail());
            }
            catch (MailException e) {
            log.error("Exception occurred when sending mail", e);
            throw new EntrepriseException("Exception occurred when sending mail to ${emailTime2it} ", e);
            }
    }
    
    @Async
    public void sendEntrepriseStatutEditionMail(String subject, String userMail, String body, String btn) {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(userMail);
                messageHelper.setSubject(subject);
                messageHelper.setText(mailContentBuilder.buildEntrepriseStatutEditionMail(body, btn),true);
            };
            try {
                mailSender.send(messagePreparator);
                log.info("Email sent successfully !!");
                //log.info(getEmail());
            }
            catch (MailException e) {
            log.error("Exception occurred when sending mail", e);
            throw new EntrepriseException("Exception occurred when sending mail to ${emailTime2it} ", e);
            }
    }
    
    @Async
    public void sendNewEntrepriseUserMail(String userMail, String password, String entreprise, List<Role> roles) {
            MimeMessagePreparator messagePreparator = mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage);
                messageHelper.setTo(userMail);
                messageHelper.setSubject("Invitation to join " +entreprise);
                messageHelper.setText(mailContentBuilder.buildNewEntrepriseUserMail(userMail, password, entreprise, roles),true);
            };
            try {
                mailSender.send(messagePreparator);
                log.info("Email sent successfully !!");
                //log.info(getEmail());
            }
            catch (MailException e) {
            log.error("Exception occurred when sending mail", e);
            throw new EntrepriseException("Exception occurred when sending mail to ${emailTime2it} ", e);
            }
    }

    public void updateAdminEmail(String newEmail){
        try {
            File file = ResourceUtils.getFile("classpath:email.properties");
            FileInputStream in = new FileInputStream(file);
            Properties props = new Properties();
            props.load(in);
            in.close();
            FileOutputStream out = new FileOutputStream(file);
            props.setProperty("emailTime2it",newEmail);
            props.store(out, null);
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }

    public EmailHost getEmailHostProperties() {
        EmailHost emailHost = new EmailHost();
        try {
            File file = ResourceUtils.getFile("classpath:application.properties");
            FileInputStream in = new FileInputStream(file);
            Properties props = new Properties();
            props.load(in);
            emailHost.setHost(props.getProperty("spring.mail.host"));
            emailHost.setPort(Integer.parseInt(props.getProperty("spring.mail.port")));
            emailHost.setUsername(props.getProperty("spring.mail.username"));
            emailHost.setPassword(props.getProperty("spring.mail.password"));
            in.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return emailHost;
    }

    public void updateEmailHostProperties(EmailHost emailHost) {
        try {
            File file = ResourceUtils.getFile("classpath:application.properties");
            FileInputStream in = new FileInputStream(file);
            Properties props = new Properties();
            props.load(in);
            in.close();
            FileOutputStream out = new FileOutputStream(file);
            props.setProperty("spring.mail.host",emailHost.getHost());
            props.setProperty("spring.mail.port", String.valueOf(emailHost.getPort()));
            props.setProperty("spring.mail.username", emailHost.getUsername());
            props.setProperty("spring.mail.password",emailHost.getPassword());
            props.store(out, null);
            out.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
