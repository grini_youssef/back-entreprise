/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.controllers;

import com.time2it.entrepriseservice.entity.Campaign;
import com.time2it.entrepriseservice.entity.ResponseMessage;
import com.time2it.entrepriseservice.repository.CampaignRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Pavilion
 */
@RestController
@AllArgsConstructor
@CrossOrigin("*")
public class CampaignController {
    
    private final CampaignRepository campaignRepo ;
    
    @GetMapping("/GetCampaigns/{id_entreprise}")
    public ResponseEntity<List<Campaign>> GetCampaigns(@PathVariable("id_entreprise") Long id_entreprise){
        List<Campaign> list = new ArrayList<>();
        for(Campaign c : campaignRepo.findAll()){
            if (c.getId_entreprise().equals(id_entreprise)){
                list.add(c);
            }
        }
        if(list.size()>0)
            return new ResponseEntity(list, HttpStatus.OK);
        else 
            return null;
        
    }
    
    @GetMapping("/GetCampaign/{id}")
    public ResponseEntity<Campaign> GetCampaign(@PathVariable("id") Long id){
        Campaign campaign = campaignRepo.findById(id).orElse(null);
        return new ResponseEntity(campaign, HttpStatus.OK);
    }
    
    @PostMapping("/createCampaign")
    public ResponseEntity<Campaign> createCampaign(@RequestBody Campaign campaign) {
        Campaign camp = campaignRepo.save(campaign);

        return new ResponseEntity(camp, HttpStatus.OK);
    }
    
    @PutMapping("/updateCampaign/{id}")
    public ResponseEntity<?> updateCampaign(@PathVariable("id") Long id, @RequestBody Campaign c){
        Campaign campaign = campaignRepo.findById(id).get();
        campaign.setName(c.getName());
        campaign.setDescription(c.getDescription());
        campaignRepo.save(campaign);

        return new ResponseEntity(new ResponseMessage("Campaign "+campaign.getName()+" has been successfully updated."), HttpStatus.OK);
    }
    
    @DeleteMapping("/deleteCampaign/{id}")
    public ResponseEntity<?> deleteCampaign(@PathVariable("id") Long id){
        Campaign campaign = campaignRepo.findById(id).get();
        campaignRepo.delete(campaign);
        
        return new ResponseEntity(new ResponseMessage("Campaign "+campaign.getName()+" has been successfully deleted."), HttpStatus.OK);
    }
    
}
