/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.time2it.entrepriseservice.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author Pavilion
 */
@Data
@NoArgsConstructor
@AllArgsConstructor

public class User  {

    private String id;
    private String username;
    private String email;
    private String firstName;
    private String lastName;
    private String password;

}
